﻿using System;
using System.Linq;
using System.Text.Json;
using CodingChallenge.Models;
using CodingChallenge.Services;
using CodingChallenge.Tests.TestInfrastructure;
using Shouldly;
using Xunit;

namespace CodingChallenge.Tests.Services
{
    public class DataServiceTests
    {
        //------------- ORDERS -------------//

        [Fact]
        public void GivenOrdersJsonString_GetOrders_ShouldLoadIntoModel()
        {
            // Arrange
            var json = "[{\"user\":\"coach\",\"drink\":\"long black\",\"size\":\"medium\"},{\"user\":\"ellis\",\"drink\":\"flat white\",\"size\":\"small\"}]";
            var testFileService = new TestFileService().WithOrdersJson(json);
            var service = new DataService(testFileService);

            // Act
            var result = service.GetOrders().ToList();

            // Assert
            result.Count.ShouldBe(2);

            result[0].User.ShouldBe("coach");
            result[0].Drink.ShouldBe("long black");
            result[0].Size.ShouldBe(DrinkSize.Medium);

            result[1].User.ShouldBe("ellis");
            result[1].Drink.ShouldBe("flat white");
            result[1].Size.ShouldBe(DrinkSize.Small);
        }

        [Fact]
        public void GivenOrdersJsonFile_LoadOrdersJson_And_GetOrders_ShouldLoadIntoModel()
        {
            // Arrange
            var service = new DataService(new FileService());

            // Act
            var result = service.GetOrders().ToList();

            // Assert
            result.Count.ShouldBe(6);

            result[0].User.ShouldBe("coach");
            result[0].Drink.ShouldBe("long black");
            result[0].Size.ShouldBe(DrinkSize.Medium);

            result[1].User.ShouldBe("ellis");
            result[1].Drink.ShouldBe("long black");
            result[1].Size.ShouldBe(DrinkSize.Small);

            result[2].User.ShouldBe("rochelle");
            result[2].Drink.ShouldBe("flat white");
            result[2].Size.ShouldBe(DrinkSize.Large);

            result[3].User.ShouldBe("coach");
            result[3].Drink.ShouldBe("flat white");
            result[3].Size.ShouldBe(DrinkSize.Large);

            result[4].User.ShouldBe("zoey");
            result[4].Drink.ShouldBe("long black");
            result[4].Size.ShouldBe(DrinkSize.Medium);

            result[5].User.ShouldBe("zoey");
            result[5].Drink.ShouldBe("short espresso");
            result[5].Size.ShouldBe(DrinkSize.Small);
        }

        [Theory]
        [InlineData("")]
        [InlineData("Invalid")]
        [InlineData("[{\"field\":\"value\"}")]
        public void GivenInvalidJsonString_GetOrders_ShouldThrowException(string json)
        {
            // Arrange
            var testFileService = new TestFileService().WithOrdersJson(json);
            var service = new DataService(testFileService);

            // Act
            // Assert
            Should.Throw<JsonException>(() => service.GetOrders());
        }

        [Fact]
        public void GivenNullJsonString_GetOrders_ShouldThrowException()
        {
            // Arrange
            var testFileService = new TestFileService().WithOrdersJson(null);
            var service = new DataService(testFileService);

            // Act
            // Assert
            Should.Throw<ArgumentNullException>(() => service.GetOrders());
        }

        [Fact]
        public void GivenInvalidDrinkSize_GetOrders_ShouldThrowException()
        {
            // Arrange
            var json = "[{\"user\":\"coach\",\"drink\":\"long black\",\"size\":\"invalid\"}]";
            var testFileService = new TestFileService().WithOrdersJson(json);
            var service = new DataService(testFileService);

            // Act
            // Assert
            var exception = Should.Throw<JsonException>(() => service.GetOrders());
            exception.Message.ShouldContain("DrinkSize");
        }

        //------------- PRICES -------------//

        [Fact]
        public void GivenPricesJsonString_GetPrices_ShouldLoadIntoModel()
        {
            // Arrange
            var json =
                "[{\"drink_name\": \"short espresso\",\"prices\": {\"small\": 3.03}}," +
                "{\"drink_name\": \"latte\",\"prices\": {\"small\": 3.50,\"medium\": 4.00,\"large\": 4.50}}]";
            var testFileService = new TestFileService().WithPricesJson(json);
            var service = new DataService(testFileService);

            // Act
            var result = service.GetPrices().ToList();

            // Assert
            result.Count.ShouldBe(2);

            result[0].Drink.ShouldBe("short espresso");
            result[0].Prices.Count.ShouldBe(1);
            result[0].Prices[DrinkSize.Small].ShouldBe(3.03m);

            result[1].Drink.ShouldBe("latte");
            result[1].Prices.Count.ShouldBe(3);
            result[1].Prices[DrinkSize.Small].ShouldBe(3.50m);
            result[1].Prices[DrinkSize.Medium].ShouldBe(4.00m);
            result[1].Prices[DrinkSize.Large].ShouldBe(4.50m);
        }

        [Fact]
        public void GivenPricesJsonFile_LoadPricesJson_And_GetPrices_ShouldLoadIntoModel()
        {
            // Arrange
            var service = new DataService(new FileService());

            // Act
            var result = service.GetPrices().ToList();

            // Assert
            result.Count.ShouldBe(6);

            result[0].Drink.ShouldBe("short espresso");
            result[0].Prices.Count.ShouldBe(1);
            result[0].Prices[DrinkSize.Small].ShouldBe(3.03m);

            result[1].Drink.ShouldBe("latte");
            result[1].Prices.Count.ShouldBe(3);
            result[1].Prices[DrinkSize.Small].ShouldBe(3.50m);
            result[1].Prices[DrinkSize.Medium].ShouldBe(4.00m);
            result[1].Prices[DrinkSize.Large].ShouldBe(4.50m);

            result[2].Drink.ShouldBe("flat white");
            result[2].Prices.Count.ShouldBe(3);
            result[2].Prices[DrinkSize.Small].ShouldBe(3.50m);
            result[2].Prices[DrinkSize.Medium].ShouldBe(4.00m);
            result[2].Prices[DrinkSize.Large].ShouldBe(4.50m);

            result[3].Drink.ShouldBe("long black");
            result[3].Prices.Count.ShouldBe(2);
            result[3].Prices[DrinkSize.Small].ShouldBe(3.25m);
            result[3].Prices[DrinkSize.Medium].ShouldBe(3.5m);

            result[4].Drink.ShouldBe("mocha");
            result[4].Prices.Count.ShouldBe(3);
            result[4].Prices[DrinkSize.Small].ShouldBe(4.00m);
            result[4].Prices[DrinkSize.Medium].ShouldBe(4.50m);
            result[4].Prices[DrinkSize.Large].ShouldBe(5.00m);

            result[5].Drink.ShouldBe("supermochacrapucaramelcream");
            result[5].Prices.Count.ShouldBe(4);
            result[5].Prices[DrinkSize.Large].ShouldBe(5.00m);
            result[5].Prices[DrinkSize.Huge].ShouldBe(5.50m);
            result[5].Prices[DrinkSize.Mega].ShouldBe(6.00m);
            result[5].Prices[DrinkSize.Ultra].ShouldBe(7.00m);
        }

        [Theory]
        [InlineData("")]
        [InlineData("Invalid")]
        [InlineData("[{\"field\":\"value\"}")]
        public void GivenInvalidJsonString_GetPrices_ShouldThrowException(string json)
        {
            // Arrange
            var testFileService = new TestFileService().WithPricesJson(json);
            var service = new DataService(testFileService);

            // Act
            // Assert
            Should.Throw<JsonException>(() => service.GetPrices());
        }

        [Fact]
        public void GivenNullJsonString_GetPrices_ShouldThrowException()
        {
            // Arrange
            var testFileService = new TestFileService().WithPricesJson(null);
            var service = new DataService(testFileService);

            // Act
            // Assert
            Should.Throw<ArgumentNullException>(() => service.GetPrices());
        }

        [Fact]
        public void GivenInvalidDrinkSize_GetPrices_ShouldThrowException()
        {
            // Arrange
            var json = "[{\"drink_name\": \"short espresso\",\"prices\": {\"invalid\": 3.03}}]";
            var testFileService = new TestFileService().WithPricesJson(json);
            var service = new DataService(testFileService);

            // Act
            // Assert
            var exception = Should.Throw<JsonException>(() => service.GetPrices());
            exception.Message.ShouldContain("DrinkSize");
        }

        //------------- Payments -------------//


        [Fact]
        public void GivenPaymentsJsonString_GetPayments_ShouldLoadIntoModel()
        {
            // Arrange
            var json = "[{\"user\": \"coach\",\"amount\": 2.50},{\"user\": \"ellis\",\"amount\": 2.60}]";
            var testFileService = new TestFileService().WithPaymentsJson(json);
            var service = new DataService(testFileService);

            // Act
            var result = service.GetPayments().ToList();

            // Assert
            result.Count.ShouldBe(2);

            result[0].User.ShouldBe("coach");
            result[0].Amount.ShouldBe(2.5m);

            result[1].User.ShouldBe("ellis");
            result[1].Amount.ShouldBe(2.6m);
        }

        [Fact]
        public void GivenPaymentsJsonFile_LoadPaymentsJson_And_GetPayments_ShouldLoadIntoModel()
        {
            // Arrange
            var service = new DataService(new FileService());

            // Act
            var result = service.GetPayments().ToList();

            // Assert
            result.Count.ShouldBe(4);

            result[0].User.ShouldBe("coach");
            result[0].Amount.ShouldBe(2.50m);

            result[1].User.ShouldBe("ellis");
            result[1].Amount.ShouldBe(2.60m);

            result[2].User.ShouldBe("rochelle");
            result[2].Amount.ShouldBe(4.50m);

            result[3].User.ShouldBe("ellis");
            result[3].Amount.ShouldBe(0.65m);
        }

        [Theory]
        [InlineData("")]
        [InlineData("Invalid")]
        [InlineData("[{\"field\":\"value\"}")]
        public void GivenInvalidJsonString_GetPayments_ShouldThrowException(string json)
        {
            // Arrange
            var testFileService = new TestFileService().WithPaymentsJson(json);
            var service = new DataService(testFileService);

            // Act
            // Assert
            Should.Throw<JsonException>(() => service.GetPayments());
        }

        [Fact]
        public void GivenNullJsonString_GetPayments_ShouldThrowException()
        {
            // Arrange
            var testFileService = new TestFileService().WithPaymentsJson(null);
            var service = new DataService(testFileService);

            // Act
            // Assert
            Should.Throw<ArgumentNullException>(() => service.GetPayments());
        }
    }
}
