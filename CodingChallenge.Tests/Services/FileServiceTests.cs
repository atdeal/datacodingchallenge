﻿using CodingChallenge.Services;
using Shouldly;
using Xunit;

namespace CodingChallenge.Tests.Services
{
    public class FileServiceTests
    {
        [Fact]
        public void GivenOrderJsonFile_LoadOrdersJson_ShouldReturnJsonString()
        {
            // Act
            var service = new FileService();
            var result = service.LoadOrdersJson();

            // Assert
            result.ShouldNotBeNullOrEmpty();
        }

        [Fact]
        public void GivenPricesJsonFile_LoadPricesJson_ShouldReturnJsonString()
        {
            // Act
            var service = new FileService();
            var result = service.LoadPricesJson();

            // Assert
            result.ShouldNotBeNullOrEmpty();
        }

        [Fact]
        public void GivenPaymentJsonFile_LoadPaymentsJson_ShouldReturnJsonString()
        {
            // Act
            var service = new FileService();
            var result = service.LoadPaymentsJson();

            // Assert
            result.ShouldNotBeNullOrEmpty();
        }
    }
}
