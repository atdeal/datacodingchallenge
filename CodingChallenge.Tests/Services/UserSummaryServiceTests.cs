﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using CodingChallenge.Models;
using CodingChallenge.Services;
using CodingChallenge.Tests.TestInfrastructure;
using CodingChallenge.Tests.TestInfrastructure.Builders;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace CodingChallenge.Tests.Services
{
    public class UserSummaryServiceTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public UserSummaryServiceTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void GivenJsonFiles_GetUserSummary_ShouldReturnDetailsPerUser()
        {
            // Arrange
            var dataService = new DataService(new FileService());
            var summaryService = new UserSummaryService(dataService);

            // Act
            var result = summaryService.GetUserSummary();

            // Assert
            CheckJsonFileData(result?.ToList());
        }

        [Fact]
        public void GivenUserNoPayments_GetUserSummary_ShouldReturnBalanceOwing()
        {
            // Arrange
            var userName = "test user";

            (DrinkSize size, decimal cost) drinkSizeA = (DrinkSize.Small, 4.00m);
            var drinkA = new PriceBuilder()
                .WithDrinkName("latte")
                .AddSize(drinkSizeA)
                .Build();
            var orderA = new OrderBuilder()
                .WithUser(userName)
                .WithDrink(drinkA.Drink)
                .WithSize(drinkSizeA.size)
                .Build();

            (DrinkSize size, decimal cost) drinkSizeB = (DrinkSize.Medium, 5.00m);
            var drinkB = new PriceBuilder()
                .WithDrinkName("latte")
                .AddSize(drinkSizeB)
                .Build();
            var orderB = new OrderBuilder()
                .WithUser(userName)
                .WithDrink(drinkB.Drink)
                .WithSize(drinkSizeB.size)
                .Build();

            var dataService = new TestDataService(
                new List<Price> { drinkA, drinkB },
                new List<Order> { orderA, orderB },
                new List<Payment>());

            var summaryService = new UserSummaryService(dataService);

            // Act
            var result = summaryService.GetUserSummary().ToList();

            // Assert
            result.Count.ShouldBe(1);
            var user = result[0];
            user.User.ShouldBe(userName);
            user.OrderTotal.ShouldBe(drinkSizeA.cost + drinkSizeB.cost);
            user.PaymentTotal.ShouldBe(0);
            user.Balance.ShouldBe(drinkSizeA.cost + drinkSizeB.cost);
        }

        [Fact]
        public void GivenUserAdditionalPayments_GetUserSummary_ShouldReturnBalanceCredit()
        {
            // Arrange
            var userName = "test user";

            (DrinkSize size, decimal cost) drinkSizeA = (DrinkSize.Small, 4.00m);
            var drinkA = new PriceBuilder()
                .WithDrinkName("latte")
                .AddSize(drinkSizeA)
                .Build();
            var orderA = new OrderBuilder()
                .WithUser(userName)
                .WithDrink(drinkA.Drink)
                .WithSize(drinkSizeA.size)
                .Build();

            (DrinkSize size, decimal cost) drinkSizeB = (DrinkSize.Medium, 5.00m);
            var drinkB = new PriceBuilder()
                .WithDrinkName("latte")
                .AddSize(drinkSizeB)
                .Build();
            var orderB = new OrderBuilder()
                .WithUser(userName)
                .WithDrink(drinkB.Drink)
                .WithSize(drinkSizeB.size)
                .Build();

            var payment = new PaymentBuilder()
                .WithUser(userName)
                .WithAmount(20)
                .Build();

            var dataService = new TestDataService(
                new List<Price> { drinkA, drinkB },
                new List<Order> { orderA, orderB },
                new List<Payment> { payment });

            var summaryService = new UserSummaryService(dataService);

            // Act
            var result = summaryService.GetUserSummary().ToList();

            // Assert
            result.Count.ShouldBe(1);
            var user = result[0];
            user.User.ShouldBe(userName);
            user.OrderTotal.ShouldBe(drinkSizeA.cost + drinkSizeB.cost);
            user.PaymentTotal.ShouldBe(payment.Amount);
            user.Balance.ShouldBe(drinkSizeA.cost + drinkSizeB.cost - payment.Amount);
        }

        [Fact]
        public void GivenUserWithPaymentsButNoOrders_GetUserSummary_ShouldReturnBalanceCredit()
        {
            // Arrange
            var userName = "test user";


            var payment = new PaymentBuilder()
                .WithUser(userName)
                .WithAmount(20)
                .Build();

            var dataService = new TestDataService(
                new List<Price>(),
                new List<Order>(),
                new List<Payment> { payment });

            var summaryService = new UserSummaryService(dataService);

            // Act
            var result = summaryService.GetUserSummary().ToList();

            // Assert
            result.Count.ShouldBe(1);
            var user = result[0];
            user.User.ShouldBe(userName);
            user.OrderTotal.ShouldBe(0);
            user.PaymentTotal.ShouldBe(payment.Amount);
            user.Balance.ShouldBe(-payment.Amount);
        }

        [Fact]
        public void GivenOrderDoesNotMatchPriceItem_GetUserSummary_ShouldThrowException()
        {
            // Arrange
            var userName = "test user";

            (DrinkSize size, decimal cost) drinkSizeA = (DrinkSize.Small, 4.00m);
            var drinkA = new PriceBuilder()
                .WithDrinkName("latte")
                .AddSize(drinkSizeA)
                .Build();
            var orderA = new OrderBuilder()
                .WithUser(userName)
                .WithDrink("invalid") // Invalid drink name
                .WithSize(drinkSizeA.size)
                .Build();

            (DrinkSize size, decimal cost) drinkSizeB = (DrinkSize.Medium, 5.00m);
            var drinkB = new PriceBuilder()
                .WithDrinkName("latte")
                .AddSize(drinkSizeB)
                .Build();
            var orderB = new OrderBuilder()
                .WithUser(userName)
                .WithDrink(drinkB.Drink)
                .WithSize(drinkSizeB.size)
                .Build();

            var payment = new PaymentBuilder()
                .WithUser(userName)
                .WithAmount(20)
                .Build();

            var dataService = new TestDataService(
                new List<Price> { drinkA, drinkB },
                new List<Order> { orderA, orderB },
                new List<Payment> { payment });

            var summaryService = new UserSummaryService(dataService);

            // Act
            // Assert
            var exception = Should.Throw<InvalidOperationException>(() => summaryService.GetUserSummary());
            _testOutputHelper.WriteLine(exception.ToString());
            exception.Message.ShouldBe(ErrorMessages.MismatchingOrdersAndPrice(1));
        }

        [Fact]
        public void GivenJsonFiles_GetUserSummaryJson_ShouldReturnJsonStringWithAllDetailsIncluded()
        {
            // Arrange
            var dataService = new DataService(new FileService());
            var summaryService = new UserSummaryService(dataService);

            // Act
            var resultJson = summaryService.GetUserSummaryJson();

            // Assert
            resultJson.ShouldNotBeNullOrEmpty();

            var result = JsonSerializer.Deserialize<ICollection<OrderSummary>>(resultJson);

            CheckJsonFileData(result?.ToList());
        }

        private static void CheckJsonFileData(List<OrderSummary> result)
        {
            result.Count.ShouldBe(4);

            result[0].User.ShouldBe("coach");
            result[0].OrderTotal.ShouldBe(8.00m);
            result[0].PaymentTotal.ShouldBe(2.50m);
            result[0].Balance.ShouldBe(5.50m);

            result[1].User.ShouldBe("ellis");
            result[1].OrderTotal.ShouldBe(3.25m);
            result[1].PaymentTotal.ShouldBe(3.250m);
            result[1].Balance.ShouldBe(0.00m);

            result[2].User.ShouldBe("rochelle");
            result[2].OrderTotal.ShouldBe(4.50m);
            result[2].PaymentTotal.ShouldBe(4.50m);
            result[2].Balance.ShouldBe(0.00m);

            result[3].User.ShouldBe("zoey");
            result[3].OrderTotal.ShouldBe(6.53m);
            result[3].PaymentTotal.ShouldBe(0.00m);
            result[3].Balance.ShouldBe(6.53m);
        }
    }
}
