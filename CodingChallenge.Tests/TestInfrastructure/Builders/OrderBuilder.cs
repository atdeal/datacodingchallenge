﻿using CodingChallenge.Models;

namespace CodingChallenge.Tests.TestInfrastructure.Builders
{
    public class OrderBuilder
    {
        private readonly Order _order;

        public OrderBuilder()
        {
            _order = new Order();
        }

        public OrderBuilder WithUser(string value)
        {
            _order.User = value;
            return this;
        }

        public OrderBuilder WithDrink(string value)
        {
            _order.Drink = value;
            return this;
        }

        public OrderBuilder WithSize(DrinkSize value)
        {
            _order.Size = value;
            return this;
        }

        public Order Build()
        {
            return _order;
        }
    }
}
