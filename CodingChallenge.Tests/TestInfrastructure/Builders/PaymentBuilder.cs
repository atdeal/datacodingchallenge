﻿using CodingChallenge.Models;

namespace CodingChallenge.Tests.TestInfrastructure.Builders
{
    public class PaymentBuilder
    {
        private readonly Payment _payment;

        public PaymentBuilder()
        {
            _payment = new Payment();
        }

        public PaymentBuilder WithUser(string value)
        {
            _payment.User = value;
            return this;
        }

        public PaymentBuilder WithAmount(decimal value)
        {
            _payment.Amount = value;
            return this;
        }

        public Payment Build()
        {
            return _payment;
        }
    }
}
