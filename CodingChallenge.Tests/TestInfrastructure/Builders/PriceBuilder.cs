﻿using System.Collections.Generic;
using CodingChallenge.Models;

namespace CodingChallenge.Tests.TestInfrastructure.Builders
{
    public class PriceBuilder
    {
        private readonly Price _price;

        public PriceBuilder()
        {
            _price = new Price { Prices = new Dictionary<DrinkSize, decimal>() };
        }

        public PriceBuilder WithDrinkName(string value)
        {
            _price.Drink = value;
            return this;
        }

        public PriceBuilder AddSize((DrinkSize size, decimal cost) value)
        {
            var (size, cost) = value;
            _price.Prices.Add(size, cost);
            return this;
        }

        public Price Build()
        {
            return _price;
        }
    }
}
