﻿using System.Collections.Generic;
using CodingChallenge.Models;
using CodingChallenge.Services;

namespace CodingChallenge.Tests.TestInfrastructure
{
    public class TestDataService : IDataService
    {
        private readonly ICollection<Price> _prices;
        private readonly ICollection<Order> _orders;
        private readonly ICollection<Payment> _payments;

        public TestDataService(ICollection<Price> prices, ICollection<Order> orders, ICollection<Payment> payments)
        {
            _prices = prices;
            _orders = orders;
            _payments = payments;
        }

        public ICollection<Price> GetPrices()
        {
            return _prices;
        }

        public ICollection<Order> GetOrders()
        {
            return _orders;
        }

        public ICollection<Payment> GetPayments()
        {
            return _payments;
        }

        public static ICollection<Price> DefaultPrices = new List<Price>
        {
            new Price
            {
                Drink = "latte",
                Prices = new Dictionary<DrinkSize, decimal> { { DrinkSize.Small, 3.00m }, { DrinkSize.Medium, 4.00m } },
            },
            new Price
            {
                Drink = "long black",
                Prices = new Dictionary<DrinkSize, decimal> { { DrinkSize.Small, 3.00m }, { DrinkSize.Medium, 4.00m } }
            }
        };
    }
}
