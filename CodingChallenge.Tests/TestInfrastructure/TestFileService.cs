﻿using CodingChallenge.Services;

namespace CodingChallenge.Tests.TestInfrastructure
{
    public class TestFileService : IFileService
    {
        private string _pricesJson;
        private string _ordersJson;
        private string _paymentsJson;

        public TestFileService WithPricesJson(string value)
        {
            _pricesJson = value;
            return this;
        }

        public TestFileService WithOrdersJson(string value)
        {
            _ordersJson = value;
            return this;
        }

        public TestFileService WithPaymentsJson(string value)
        {
            _paymentsJson = value;
            return this;
        }

        public string LoadPricesJson()
        {
            return _pricesJson;
        }

        public string LoadOrdersJson()
        {
            return _ordersJson;
        }

        public string LoadPaymentsJson()
        {
            return _paymentsJson;
        }
    }
}
