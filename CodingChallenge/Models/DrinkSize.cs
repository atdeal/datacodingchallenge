﻿namespace CodingChallenge.Models
{
    public enum DrinkSize
    {
        Small,
        Medium,
        Large,
        Huge,
        Mega,
        Ultra
    }
}
