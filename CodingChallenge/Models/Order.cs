﻿using System.Text.Json.Serialization;

namespace CodingChallenge.Models
{
    public class Order
    {
        [JsonPropertyName("user")] public string User { get; set; }

        [JsonPropertyName("drink")] public string Drink { get; set; }

        [JsonPropertyName("size")] public DrinkSize Size { get; set; }
    }
}
