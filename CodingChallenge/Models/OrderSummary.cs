﻿using System;
using System.Text.Json.Serialization;

namespace CodingChallenge.Models
{
    public class OrderSummary : IEquatable<OrderSummary>
    {
        [JsonPropertyName("user")] public string User { get; set; }
        [JsonPropertyName("order_total")] public decimal OrderTotal { get; set; }
        [JsonPropertyName("payment_total")] public decimal PaymentTotal { get; set; }
        [JsonPropertyName("balance")] public decimal Balance { get; set; }

        public bool Equals(OrderSummary other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return User == other.User && OrderTotal == other.OrderTotal && PaymentTotal == other.PaymentTotal && Balance == other.Balance;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OrderSummary) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(User, OrderTotal, PaymentTotal, Balance);
        }
    }
}
