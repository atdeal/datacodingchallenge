﻿using System.Text.Json.Serialization;

namespace CodingChallenge.Models
{
    public class Payment
    {
        [JsonPropertyName("user")] public string User { get; set; }

        [JsonPropertyName("amount")] public decimal Amount { get; set; }
    }
}
