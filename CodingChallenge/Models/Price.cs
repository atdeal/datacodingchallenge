﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace CodingChallenge.Models
{
    public class Price
    {
        [JsonPropertyName("drink_name")] public string Drink { get; set; }

        [JsonPropertyName("prices")] public Dictionary<DrinkSize, decimal> Prices { get; set; }
    }
}
