﻿using System;
using CodingChallenge.Services;

namespace CodingChallenge
{
    public class MudbathMochaApplication
    {
        private readonly IUserSummaryService _userSummaryService;

        public MudbathMochaApplication(IUserSummaryService userSummaryService)
        {
            _userSummaryService = userSummaryService;
        }

        public void Run()
        {
            // Get the user summary and convert to json
            var result = _userSummaryService.GetUserSummaryJson();
            Console.WriteLine(result);
        }
    }
}
