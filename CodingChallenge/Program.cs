﻿using CodingChallenge.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CodingChallenge
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // Create service collection and configure our services for dependency injection
            IServiceCollection services = new ServiceCollection();

            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IDataService, DataService>();
            services.AddTransient<IUserSummaryService, UserSummaryService>();
            services.AddTransient<MudbathMochaApplication>();

            // Generate a provider
            var serviceProvider = services.BuildServiceProvider();

            // Start the application
            serviceProvider.GetService<MudbathMochaApplication>()?.Run();
        }
    }
}
