# Mudbath Mocha

We really like coffee.

So we built an app to fetch coffee for people from our favourite barista.

The app keeps track of coffee ordered; what the balance is for each user; what users have paid for already; and what is still owed.

## Architecture

This application runs as console application, however is structure to be used in any .Net Core application such as a web API using the UserSummaryService.

####  UserSummaryService
The user summary service is the primary service, utilising the data service to load in all required data and generate the per user summary.

#### DataService
The data service is responsible for loading the three arrays of data in the correct models. The implementation used here utilises the File service to load the json objects.
It is anticipated that this service would be replaced to utilise entity framework or similar data loader.

#### FileService
This is used in the Data Service to provide a raw json string.

## Data
All data is provided in the 3 tables below.
- `Data/prices.json` - provided by our barista. Has details of what beverages are available, and what their prices are.
- `Data/orders.json` - list of beverages ordered by users of the app.
- `Data/payments.json` - list of payments made by users paying for items they have purchased.

## Building Solution

The .NET Core CLI, which is installed with [the .NET Core SDK](https://www.microsoft.com/net/download) is required to build the application.
Once installed run the commands below:

```console
dotnet build
dotnet run
```

## Tests

Tests can be run using the built in .NET Core test runner.

```shell
dotnet test
```
