﻿using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using CodingChallenge.Models;

namespace CodingChallenge.Services
{
    public class DataService : IDataService
    {
        private readonly IFileService _fileService;

        public DataService(IFileService fileService)
        {
            _fileService = fileService;
        }

        private readonly JsonSerializerOptions _jsonOptions = new JsonSerializerOptions
        {
            WriteIndented = true,
            Converters =
            {
                new JsonStringEnumConverter(JsonNamingPolicy.CamelCase)
            }
        };

        public ICollection<Price> GetPrices()
        {
            var json = _fileService.LoadPricesJson();
            return JsonSerializer.Deserialize<ICollection<Price>>(json, _jsonOptions);
        }


        public ICollection<Order> GetOrders()
        {
            var json = _fileService.LoadOrdersJson();
            return JsonSerializer.Deserialize<ICollection<Order>>(json, _jsonOptions);
        }

        public ICollection<Payment> GetPayments()
        {
            var json = _fileService.LoadPaymentsJson();
            return JsonSerializer.Deserialize<ICollection<Payment>>(json, _jsonOptions);
        }
    }
}
