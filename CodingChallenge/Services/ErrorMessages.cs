﻿namespace CodingChallenge.Services
{
    public static class ErrorMessages
    {
        public static string MismatchingOrdersAndPrice(int number) => $"{number} order(s) do not have a listed price for the given drink & size";
    }
}
