﻿using System.IO;

namespace CodingChallenge.Services
{
    public class FileService : IFileService
    {
        public string LoadPricesJson()
        {
            return File.ReadAllText(@"Data\prices.json");
        }

        public string LoadOrdersJson()
        {
            return File.ReadAllText(@"Data\orders.json");
        }

        public string LoadPaymentsJson()
        {
            return File.ReadAllText(@"Data\payments.json");
        }
    }
}
