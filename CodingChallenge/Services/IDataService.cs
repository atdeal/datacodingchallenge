﻿using System.Collections.Generic;
using CodingChallenge.Models;

namespace CodingChallenge.Services
{
    public interface IDataService
    {
        ICollection<Price> GetPrices();

        ICollection<Order> GetOrders();

        ICollection<Payment> GetPayments();
    }
}
