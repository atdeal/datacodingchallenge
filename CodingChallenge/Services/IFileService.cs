﻿namespace CodingChallenge.Services
{
    public interface IFileService
    {
        string LoadPricesJson();
        string LoadOrdersJson();
        string LoadPaymentsJson();
    }
}
