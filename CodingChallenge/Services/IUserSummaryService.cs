﻿using System.Collections.Generic;

namespace CodingChallenge.Services
{
    public interface IUserSummaryService
    {
        ICollection<Models.OrderSummary> GetUserSummary();
        string GetUserSummaryJson();
    }
}
