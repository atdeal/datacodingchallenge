﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using CodingChallenge.Models;

namespace CodingChallenge.Services
{
    public class UserSummaryService : IUserSummaryService
    {
        private readonly IDataService _dataService;

        public UserSummaryService(IDataService dataService)
        {
            _dataService = dataService;
        }

        public string GetUserSummaryJson()
        {
            var result = GetUserSummary();
            return JsonSerializer.Serialize(result, new JsonSerializerOptions { WriteIndented = true });
        }

        public ICollection<OrderSummary> GetUserSummary()
        {
            var prices = _dataService.GetPrices().ToList();
            var pricesExpanded = prices.SelectMany(s =>
                s.Prices.Select(p => new
                {
                    s.Drink,
                    Size = p.Key,
                    Cost = p.Value
                })
            );

            var orders = _dataService.GetOrders().ToList();

            var ordersWithPrice = (from order in orders
                                   join price in pricesExpanded on new { order.Drink, order.Size } equals new { price.Drink, price.Size }
                                   select new
                                   {
                                       order.User,
                                       order.Drink,
                                       order.Size,
                                       price.Cost
                                   }).ToList();

            if (orders.Count != ordersWithPrice.Count)
                throw new InvalidOperationException(ErrorMessages.MismatchingOrdersAndPrice(orders.Count - ordersWithPrice.Count));

            var userOrders = ordersWithPrice.ToList().GroupBy(o => o.User).ToList();

            var payments = _dataService.GetPayments();
            var userPayments = payments.GroupBy(p => p.User).ToList();

            // Full outer join to get all users who have made an order or a payment
            var leftOuterJoin = from order in userOrders
                                join payment in userPayments
                                    on order.Key equals payment.Key into temp
                                from payment in temp.DefaultIfEmpty()
                                let orderTotal = order.Sum(o => o.Cost)
                                let paymentTotal = payment?.Sum(p => p.Amount) ?? 0
                                select new OrderSummary
                                {
                                    User = order.Key,
                                    OrderTotal = orderTotal,
                                    PaymentTotal = paymentTotal,
                                    Balance = orderTotal - paymentTotal
                                };

            var rightOuterJoin = from payment in userPayments
                                 join order in userOrders
                                     on payment.Key equals order.Key into temp
                                 from order in temp.DefaultIfEmpty()
                                 let paymentTotal = payment.Sum(p => p.Amount)
                                 let orderTotal = order?.Sum(o => o.Cost) ?? 0
                                 select new OrderSummary
                                 {
                                     User = payment.Key,
                                     OrderTotal = orderTotal,
                                     PaymentTotal = paymentTotal,
                                     Balance = orderTotal - paymentTotal
                                 };

            var fullOuterJoin = leftOuterJoin.Union(rightOuterJoin, new OrderSummaryNameComparer());

            return fullOuterJoin.ToList();
        }

        private class OrderSummaryNameComparer : IEqualityComparer<OrderSummary>
        {
            public bool Equals(OrderSummary x, OrderSummary y)
            {
                return x?.User == y?.User;
            }

            public int GetHashCode(OrderSummary product)
            {
                return product.User.GetHashCode();
            }
        }
    }
}
